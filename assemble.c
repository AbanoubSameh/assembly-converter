/*
 * assemble.c
 *
 *  Created on: Aug 3, 2018
 *      Author: abanoub
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <bits/types/FILE.h>

#define SIXTYFOUR 64
#define THIRTYTWO 32
#define SIXTEEN 16
#define FOUR 4

#define COMMENT "comment"
#define LABEL "label"
#define GAS "gas"
#define INSTRUCTION "instruction"

#define PLUS "+"
#define TIMES "*"
#define REL "[rel "
#define ASM ".asm"

#define BYTE " byte"
#define WORD " word"
#define DWORD " dword"
#define QWORD " qword"
#define TWORD " tword"

int solveWord(char* line, char* word) {

	char firstWord[THIRTYTWO];
	int index = 0, windex = 0;

	while (line[index] != 10 && line[index] != 0) {

		if (line[index] == 9 || line[index] == 32 || line[index] == 44) {

			if (windex != 0) {
				break;
			}

		} else if (line[index] == 34) {

			firstWord[windex] = line[index];
			windex++;
			index++;

			while (line[index] != 34) {

				firstWord[windex] = line[index];
				windex++;
				index++;

			}

			firstWord[windex] = line[index];
			windex++;
			index++;

		}  else if (line[index] == 40) {

			firstWord[windex] = line[index];
			windex++;
			index++;

			while (line[index] != 41) {

				firstWord[windex] = line[index];
				windex++;
				index++;

			}

			firstWord[windex] = line[index];
			windex++;
			index++;

		} else {
			firstWord[windex] = line[index];
			windex++;
		}

		index++;

	}

	firstWord[windex] = 0;

	strcpy(word, firstWord);

	return index;

}

void solvePointerBeginning(char* operand) {

	while(operand[0] == 37 || operand[0] == 36 || operand[0] == 32 || operand[0] == 46) {
		memmove(operand, operand + 1, strlen(operand));
	}

}

void solvePointer(char* pointer) {

//	printf("%s\n", pointer);

	char a[SIXTEEN], b[SIXTEEN], c[SIXTEEN], d[SIXTEEN], end[SIXTEEN];
	char* put = a;
	int index = 0, pindex = 0, eindex = 1;

	while (pointer[index] != ')') {

		if (pointer[index] == '(' || pointer[index] == ',') {
			*(put + pindex) = 0;
			pindex = 0;
			if (put == a) {
				put = b;
			} else if (put == b) {
				put = c;
			} else if (put == c) {
				put = d;
			} else if (put == d) {
				break;
			}

		}

		*(put + pindex) = pointer[index];
		index++;
		pindex++;

	}

	if (put == a) {
		b[0] = 0;
		c[0] = 0;
		d[0] = 0;
	} else if (put == b) {
		c[0] = 0;
		d[0] = 0;
	} else if (put == c) {
		d[0] = 0;
	}

	*(put + pindex) = 0;

	if (strlen(a) != 0) {
		solvePointerBeginning(a);
	}
	if (strlen(b) != 0) {
		memmove(b, b + 1, strlen(b));
		solvePointerBeginning(b);
	}
	if (strlen(c) != 0) {
		memmove(c, c + 1, strlen(c));
		solvePointerBeginning(c);
	}
	if (strlen(d) != 0) {
		memmove(d, d + 1, strlen(d));
		solvePointerBeginning(d);
	}

	if (strstr(a, "rip")) {
		strcpy(a, "");
		memmove(end, REL, 5);
		eindex += 4;
	}
	if (strstr(b, "rip")) {
		strcpy(b, "");
		memmove(end, REL, 5);
		eindex += 4;
	}
	if (strstr(c, "rip")) {
		strcpy(c, "");
		memmove(end, REL, 5);
		eindex += 4;
	}
	if (strstr(d, "rip")) {
		strcpy(d, "");
		memmove(end, REL, 5);
		eindex += 4;
	}

	end[0] = '[';


	if (strlen(a) != 0) {
	//	printf("first: %s\n", a);
		if (a[0] != '-') {
			memmove(end + eindex, PLUS, 1);
			eindex++;
		}
		memmove(end + eindex, a, strlen(a) + 1);
		eindex += strlen(a);
	}
	if (strlen(b) != 0) {
	//	printf("second: %s\n", b);
		if (b[0] != '-') {
			memmove(end + eindex, PLUS, 1);
			eindex++;
		}
		memmove(end + eindex, b, strlen(b) + 1);
		eindex += strlen(b);
	}
	if (strlen(c) != 0) {
	//	printf("third: %s\n", c);
		if (c[0] != '-') {
			memmove(end + eindex, PLUS, 1);
			eindex++;
		}
		memmove(end + eindex, c, strlen(c) + 1);
		eindex += strlen(c);
	}
	if (strlen(d) != 0) {
	//	printf("fourth: %s\n", d);
		if (d[0] != '-') {
			memmove(end + eindex, TIMES, 1);
			eindex++;
		}
		memmove(end + eindex, d, strlen(d) + 1);
		eindex += strlen(d);
	}
	end[eindex] = ']';
	end[eindex + 1] = '\0';

//	printf("%s\n", end);
	memmove(pointer, end, strlen(end) + 1);

}

int exported(char* word, FILE* file){

	char line[SIXTYFOUR];

	rewind(file);
	while (fgets(line, SIXTYFOUR, file)){

		solveWord(line + 7, line);
		if(!strcmp(line, word)){
			return 1;
		}
	}


	return 0;
}

void solveOperand(char* operand) {

	if (operand[0] == 37 || operand[0] == 36) {
		memmove(operand, operand + 1, strlen(operand));
	}

}

void solveExtern(char* word) {

	for (int i = 0; i < strlen(word) - 1; i++) {

		if (word[i] == 64) {
			word[i] = 0;
		}

	}

}

char* determination(char* operand){

	char *size;
	if(operand[strlen(operand) - 1] == 'b'){
		size = BYTE;
	}else if (operand[strlen(operand) - 1] == 's' || operand[strlen(operand) - 1] == 'w'){
		size = WORD;
	}else if (operand[strlen(operand) - 1] == 'l'){
		size = DWORD;
	}else if (operand[strlen(operand) - 1] == 'q'){
		size = QWORD;
	}else if (operand[strlen(operand) - 1] == 't'){
		size = TWORD;
	}

	return size;
}

char* solveExtend(char* word) {

	word[strlen(word) - 1] = 0;
	char* size = determination(word);

//	word[strlen(word) - 2] = 'z';
	word[strlen(word) - 1] = 'x';
	return size;

}

void solveCextended(char* word){

	if(strstr(word, "cbtw")){
		strcpy(word, "cbw");
	}else if(strstr(word, "cwtl")){
		strcpy(word, "cwde");
	}else if(strstr(word, "cwtd")){
		strcpy(word, "cwd");
	}else if(strstr(word, "cltd")){
		strcpy(word, "cdq");
	}else if(strstr(word, "cltq")){
		strcpy(word, "cdqe");
	}else if(strstr(word, "cqto")){
		strcpy(word, "cqo");
	}

}

char* trimOperand(char* operand) {

	char* size = determination(operand);

	operand[strlen(operand) - 1] = 0;

	return size;
}

void solveComment(char* line, FILE* file) {

//	printf("came here\n");
	char word[THIRTYTWO];
	solveWord(line, word);

	if (strstr(line, "//") != NULL) {
		printf(";%s", strstr(line, word) + 2);
		fprintf(file, ";%s", strstr(line, word) + 2);
	} else if (strstr(line, "/*") != NULL) {
		printf(";%s", strstr(line, word) + 2);
		fprintf(file, ";%s", strstr(line, word) + 2);
	} else if (strstr(line, "*/") != NULL) {
		printf(";%s", strstr(line, word) + 2);
		fprintf(file, ";%s", strstr(line, word) + 2);
	} else if (strstr(line, "#") != NULL) {
		printf(";%s", strstr(line, word) + 1);
		fprintf(file, ";%s", strstr(line, word) + 1);
	} else if (strstr(line, "*") != NULL) {
		printf(";%s", strstr(line, word) + 1);
		fprintf(file, ";%s", strstr(line, word) + 1);
	} else {
		printf(";%s", strstr(line, word));
		fprintf(file, ";%s", strstr(line, word));
	}

}

void solveLabel(char* line) {

	char word[THIRTYTWO];
	solveWord(line, word);

	if (word[0] == 46) {
//		printf("%s\n", word + 1);
		memmove(line, line + 1, strlen(line));
	} // else {
//		printf("%s\n", word);
//	}

}

void solveGas(char* line, FILE* file) {

	char word[THIRTYTWO];
	int size = solveWord(line, word), offset = size;

	if (strstr(word, ".text") || strstr(word, ".data") || strstr(word, ".bss")
			|| strstr(word, ".rodata")) {
		printf("section %s\n", word);
		fprintf(file, "section %s\n", word);

	} else if (strstr(word, ".section")) {

		size = solveWord(line + offset, word);
		offset += size;

		printf("section %s\n", word);
		fprintf(file, "section %s\n", word);

	} else if (strstr(word, "globl")) {

		size = solveWord(line + offset, word);
		offset += size;

		printf("\tglobal %s\n", word);
		fprintf(file, "\tglobal %s\n", word);

	} else if (strstr(word, ".string")) {

		size = solveWord(line + offset, word);
		offset += size;

		word[0] = '`';
		word[strlen(word) - 1] = '`';

//		printf("%d\n", size);
		printf("\tdb %s,0\n", word);
		fprintf(file, "\tdb %s,0\n", word);

	}

}

void solveInstruction(char* line, FILE* file, FILE* file2) {

	char word[THIRTYTWO], word2[THIRTYTWO], word3[THIRTYTWO];
	int size = solveWord(line, word), offset = size;

	size = solveWord(line + offset, word2);
	offset += size;
	size = solveWord(line + offset, word3);
	offset += size;

	if(strstr(word2, ":")){
		word2[0] = '[';
		word2[strlen(word2)] = ']';
	}

	if(strstr(word3, ":")){
		word3[0] = '[';
		word3[strlen(word3)] = ']';
	}

	if (strstr(word2, "(")) {

		if(strstr(word2, "stdin") && !exported(word2, file2)){
			fprintf(file2, "extern stdin\n");
		}

		solvePointer(word2);
	}

	if (strstr(word3, "(")) {

		if(strstr(word3, "stdin") && !exported(word3, file2)){
			fprintf(file2, "extern stdin\n");
		}

		solvePointer(word3);
	}

	if (word[0] == 'j' || strstr(word, "call")) {

		solveOperand(word2);
		solveLabel(word2);

		if (strstr(word2, "@PLT")) {

			solveExtern(word2);
			printf("\t%s %s wrt ..plt\n", word, word2);
			fprintf(file, "\t%s %s wrt ..plt\n", word, word2);

			if(!exported(word2, file2)){
				fprintf(file2, "extern %s\n", word2);
			}

		} else {
			printf("\t%s %s\n", word, word2);
			fprintf(file, "\t%s %s\n", word, word2);
		}

	}else if (strstr(word, "push") || strstr(word, "pop")) {

		trimOperand(word);
		solveOperand(word2);
		solveLabel(word2);

		printf("\t%s %s\n", word, word2);
		fprintf(file, "\t%s %s\n", word, word2);

	} else if ((strstr(word, "movz") || strstr(word, "movs")) && strlen(word) > 5) {

		char* temp = solveExtend(word);

		solveOperand(word2);
		solveOperand(word3);

		if(strstr(word2, "[")){
			printf("\t%s %s,%s %s\n", word, word3, temp, word2);
			fprintf(file, "\t%s %s,%s %s\n", word, word3, temp, word2);
		}else{
			printf("\t%s %s, %s\n", word, word3, word2);
			fprintf(file, "\t%s %s, %s\n", word, word3, word2);
		}

	} else if (strlen(word2) == 0) {

		if(strstr(word, "c")){
			solveCextended(word);
		}

		printf("\t%s\n", word);
		fprintf(file, "\t%s\n", word);

	} else {

//		printf("first argument: %s\n", word);
//		printf("second argument: %s\n", word2);

		if (strlen(word3) != 0) {
//			printf("third argument: %s\n", word3);

//			printf("%s\n", word + strlen(word) - 2);
			if (strstr(word, "xchg") == NULL && strstr(word, "pxor") == NULL && strcmp(word + (strlen(word) - 2), "sd") != 0) {

				if ((strstr(word2, "[") && strstr(word3, "$"))
						|| (strstr(word3, "[") && strstr(word2, "$"))) {
					strcat(word, trimOperand(word));
				}else{
					trimOperand(word);
				}

			}

			solveOperand(word);
			solveOperand(word2);
			solveOperand(word3);

			printf("\t%s %s, %s\n", word, word3, word2);
			fprintf(file, "\t%s %s, %s\n", word, word3, word2);

		} else {

			if (strstr(word, "idiv")) {
				word[4] = 0;
				solveOperand(word2);
			}

			if (strstr(word, "neg")) {
				word[3] = 0;
				solveOperand(word2);
			}

			if (strstr(word, "not")) {
				word[3] = 0;
				solveOperand(word2);
			}

			printf("\t%s %s\n", word, word2);
			fprintf(file, "\t%s %s\n", word, word2);

		}

	}

}

void solve(char* line, FILE* file, FILE* file2) {

//	printf("%s", line);
	char firstWord[THIRTYTWO];
	solveWord(line, firstWord);
//	printf("%s\n", firstWord);

//	printf("first word: %s\n", firstWord);

	if (strstr(firstWord, "//") != NULL || strstr(firstWord, "#") != NULL) {
//		printf("solver: %s\n", COMMENT);
		solveComment(line, file);
	} else if (strstr(firstWord, ":") != NULL) {
//		printf("solver: %s\n", LABEL);
		solveLabel(line);
		printf("%s", line);
		fprintf(file, "%s", line);
	} else if (strstr(firstWord, ".") != NULL) {
//		printf("solver: %s\n", GAS);
		solveGas(line, file);
	} else {
//		printf("solver: %s\n", INSTRUCTION);
		solveInstruction(line, file, file2);
	}

}

int getName(char* input){

	int i;
	for(i = 0; i < strlen(input); i++){

		if(input[i] == '.'){
			break;
		}

	}

	return i;

}

int main(int argc, char **argv) {

	int size;
	char buffer[SIXTYFOUR], name[THIRTYTWO], outName[THIRTYTWO];

	if (argc > 1) {

		for (int i = 1; i < argc; i++) {

			if (!strcmp(argv[i - 1], "-o")) {
//              printf("The output name: %s\n", argv[i]);
				strcpy(outName, argv[i]);
			}

			if (strcmp(argv[i - 1], "-o") && strcmp(argv[i], "-o")) {
//              printf("The input name: %s\n", argv[i]);
				strcpy(name, argv[i]);
			}

		}

	} else {
		name[0] = 0;
	}

	if (strlen(name) == 0 || name[0] == 0) {
		printf("Enter file name\n");
		fgets(name, THIRTYTWO, stdin);
		name[strlen(name) - 1] = 0;
	}

	if (strlen(name) == 0 || name[0] == 0) {
		printf("Sorry, an error occurred\t:)\n");
		return -1;
	} else {

		if(access(name, F_OK) != 0) {
		    printf("File doesn't exist\n");
		    return -1;
		}

		if(access(name, R_OK) != 0) {
		    printf("Don't have permission to read file\n");
		    return -1;
		}

		if (strlen(outName) == 0 || outName[0] == 0) {
			strcpy(outName, name);
//			printf("%s\n", outName);
//			printf("%d\n", getName(name));
			memmove(outName + getName(name), ASM, 5);
			outName[strlen(outName)] = 0;
//			printf("%s\n", outName);
		}

	}

	FILE* file = fopen(name, "r");

	if (file) {

		fseek(file, 0, SEEK_END);
		size = ftell(file);

		if (size == 0) {
			printf("file is empty\n");
			return -1;
		} else {
			fseek(file, 0, SEEK_SET);
		}

		FILE* outFile = fopen(outName, "w+");
		strcat(outName, "2");

		FILE* outFile2 = fopen(outName, "w+");

		if(!outFile || !outFile2){
			printf("Couldn't write to file\n");
			return -1;
		}

		while (fgets(buffer, SIXTYFOUR, file)) {

			if (strstr(buffer, "/*") != NULL && strstr(buffer, "\"") == NULL) {

				while (!strstr(buffer, "*/")) {
					solveComment(buffer, outFile);
					memset(buffer, 0, SIXTYFOUR);
					fgets(buffer, SIXTYFOUR, file);
				}

				solveComment(buffer, outFile);
				memset(buffer, 0, SIXTYFOUR);
				fgets(buffer, SIXTYFOUR, file);

			}

			//		printf("%s", buffer);
			solve(buffer, outFile2, outFile);

			memset(buffer, 0, SIXTYFOUR);

		}

		rewind(outFile2);
		fseek(outFile, 0, SEEK_END);
		fprintf(outFile, "\n");

		while(fgets(buffer, SIXTYFOUR, outFile2)){
			fprintf(outFile, "%s", buffer);
		}

		if(unlink(outName)){
			printf("Could not delete part 2\n");
		}

		fclose(file);
		fclose(outFile);
		fclose(outFile2);

	} else {

		printf("Sorry, an error occurred\t:)\n");
		return -1;

	}

//	char test[] = "Hello my name is";
//	solve(test);

}
